const customer = require('../src/controller/s1.controller');

const app = require('../src/index');
const supertest = require('supertest');
const request  = supertest(app);

/*test('Address: if Street not null', () =>{
    let address ={
        "address":"123 test",
        "unit":"1",
        "poBox":"poBox test",
        "privateBag":"dd",
    }
    expect(processAddress(address)).toBe('123 test');
})*/


test('Address: if Street is null', () =>{
    let address ={
        "address":"",
        "unit":"1",
        "poBox":"poBox test",
        "privateBag":"dd",
    }
    expect(customer.processAddress(address)).toBe('poBox test');
})