"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const s1_controller_1 = require("./controller/s1.controller");
const router = express_1.default.Router();
router.post('/customer', s1_controller_1.pushDataToS3);
exports.default = router;
//# sourceMappingURL=routes.js.map