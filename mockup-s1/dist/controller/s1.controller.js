"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const config_1 = require("../config");
exports.pushDataToS3 = (req, res) => {
    axios_1.default
        .post(config_1.config.mappingAPI, req.body)
        .then(function (response) {
        return res.status(200).send(response.data);
    })
        .catch(function (error) {
        console.error(error);
        throw error;
    });
};
//# sourceMappingURL=s1.controller.js.map