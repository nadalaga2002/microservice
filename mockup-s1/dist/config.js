"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = {
    mappingAPI: process.env.MAPPING_API || 'http://localhost:7050/s2',
    env: process.env.NODE_ENV || 'development',
    port: process.env.PORT || 8060
};
//# sourceMappingURL=config.js.map