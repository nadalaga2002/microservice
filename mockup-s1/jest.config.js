module.exports = {

    
    moduleFileExtensions: ['ts', 'tsx','js'],
    transform: {
        '\\.(ts|tsx)$': 'ts-jest',
    },
    testRegex: '.test.(ts|js)$',
    modulePaths: ['<rootDir>/src/'],
    modulePathIgnorePatterns: ['node_modules','__tests__','dist'],
    transformIgnorePatterns: [`node_modules`],
}


