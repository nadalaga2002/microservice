const config = require('src/config');
const { Pool } = require('pg');

const pool = new Pool({
  user: config.dbUser,
  host: config.dbHOST,
  database: config.dbDatabase,
  password: config.dbPassword,
  port: config.dbPORT
});

module.exports = {
  query: (text, params, callback) => {
    return pool.query(text, params, callback)
  },
}