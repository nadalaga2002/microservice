require('dotenv').config();
//require('module-alias/register');

import express from 'express';
import bodyParser from  'body-parser';
import  { config}  from './config';
import routes from './routes';

const app = express();

//bodyparser setup
//support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({ extended: true }));
// parse various different custom JSON types as JSON
app.use(bodyParser.json({ type: 'application/*+json' }));
app.use(bodyParser.raw({ type: 'application/vnd.custom-type' }));
// parse an HTML body into a string
app.use(bodyParser.text({ type: 'text/html' }));

// support parsing of application/json type post data
app.use(bodyParser.json());


// PUBLIC ROUTES
// =============================================
app.use('/', routes);

//app.use(routes);

// NOT FOUND HANDLER
// =============================================
app.use(function(req, res) {
  return res.status(404).json({
    message: 'Not found',
    error: '400'
  });
});

// ERROR HANDLER
// =============================================
app.use((req, res) => {
  //logger.error(error);

  return res.status(500).json({
    message: 'Server Internal Error',
    error: '500'
  });
});

app.get('/', (req, res) => {
  res.send(`Node and express server is running on PORT ${config.port}`);
});

module.exports = app
