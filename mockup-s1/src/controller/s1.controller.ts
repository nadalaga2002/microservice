import axios from 'axios';
import {Request, Response} from 'express';
import  {config}  from '../config';

 export const pushDataToS3 = (req :Request, res: Response) => {
  console.log(config.mappingAPI);
  
  axios
    .post(config.mappingAPI, req.body)
    .then((response:any) => {
      if(res.statusCode.toString()==='200'){
        return res.status(200).send(response.data);
      }else{
        return res.status(400).send(response.error);
      }
    })
    .catch(function(error: any) {
      if(error.response){
        return res.status(500).send(error.response);
      }
      
      console.error(error);
      throw error;
    });
 
};

export const processAddress = function (billingAddress:any) {
  if (billingAddress.address != "") {
      if (billingAddress.unit != "")
          return "Unit " + billingAddress.unit + "," + billingAddress.address;
      else
          return billingAddress.address;
  }
  else if (billingAddress.poBox != "")
      return billingAddress.poBox;
};