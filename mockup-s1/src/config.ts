export const config = {
    mappingAPI: process.env.MAPPING_API || 'http://mockup-s2-hoangnm.4b63.pro-ap-southeast-2.openshiftapps.com/s2',
    env: process.env.NODE_ENV || 'development',
    port: process.env.PORT || 8060
};
