"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require('dotenv').config();
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const config_1 = require("./config");
const routes_1 = __importDefault(require("./routes"));
const app = express_1.default();
app.use(body_parser_1.default.urlencoded({ extended: true }));
app.use(body_parser_1.default.json({ type: 'application/*+json' }));
app.use(body_parser_1.default.raw({ type: 'application/vnd.custom-type' }));
app.use(body_parser_1.default.text({ type: 'text/html' }));
app.use(body_parser_1.default.json());
app.use('/', routes_1.default);
app.use(function (req, res) {
    return res.status(404).json({
        message: 'Not found',
        error: '400'
    });
});
app.use((req, res) => {
    return res.status(500).json({
        message: 'Server Internal Error',
        error: '500'
    });
});
app.get('/', (req, res) => {
    res.send(`Node and express server is running on PORT ${config_1.config.port}`);
});
app.listen(config_1.config.port, () => console.info(`Your server is running on port ${config_1.config.port}`));
//# sourceMappingURL=index.js.map