"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const s2_controller_1 = require("./controller/s2.controller");
const router = express_1.default.Router();
const mapping_middeware_1 = require("./lib/middleware/mapping.middeware");
router.post('/s2', mapping_middeware_1.mappingData, s2_controller_1.sendRequestToAPI);
exports.default = router;
//# sourceMappingURL=routes.js.map