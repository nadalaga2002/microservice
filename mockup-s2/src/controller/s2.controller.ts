import axios from 'axios';
import {Request, Response} from 'express';
import {config} from '../config';

export const sendRequestToAPI = (req :Request, res: Response) => {
   /* axios
    .post(config.APIEndpoint+'/s3', req.body)
    .then(function(response) {
      return res.status(200).send(response.data);
    })
    .catch(function(error: any) {
      console.error(error);
      throw error;
    });*/
  
  return res.status(200).send(req.body);
   
};
