export const config = {
    APIEndpoint: process.env.APIENPOINT || 'http://localhost:7020',
    env: process.env.NODE_ENV || 'development',
    port: process.env.PORT || 8060
};
