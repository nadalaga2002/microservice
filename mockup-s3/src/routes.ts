import express from 'express';
import { insertDB } from './controller/s3.controller';
const router = express.Router();

router.post('/s3', insertDB);

export default router;
